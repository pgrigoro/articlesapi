package site.maciel.articles.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import site.maciel.articles.domain.Tag;

public interface TagRepository extends JpaRepository<Tag, Long> {

}
