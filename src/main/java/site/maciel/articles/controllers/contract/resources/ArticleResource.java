package site.maciel.articles.controllers.contract.resources;

import java.net.URL;

import org.springframework.hateoas.ResourceSupport;

import lombok.Data;

@Data
public class ArticleResource extends ResourceSupport {

	public String name;

	public URL url;

}
