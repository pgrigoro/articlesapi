package site.maciel.articles.controllers.contract.resources;

import org.springframework.hateoas.ResourceSupport;

import lombok.Data;

@Data
public class TagCollectionResource extends ResourceSupport {
	
	public Long count;
	
	public Long offset;

	public Long limit;

}
