package site.maciel.articles.controllers.contract.resources;

import org.springframework.hateoas.ResourceSupport;

import lombok.Data;

@Data
public class HomeResource extends ResourceSupport {
	
	public String name;

}
