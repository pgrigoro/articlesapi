package site.maciel.articles.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import site.maciel.articles.controllers.contract.resources.TagCollectionResource;
import site.maciel.articles.persistence.repositories.TagRepository;

@RestController
@RequestMapping(path = "/tags", produces = "application/hal+json")
public class TagsController {

	private TagRepository _tagRepository;

	@Autowired
	public TagsController(TagRepository tagRepository) {
		_tagRepository = tagRepository;
	}

	@GetMapping
	public ResponseEntity<TagCollectionResource> GetTagCollection(@RequestParam(defaultValue = "0") Long offset,
			@RequestParam(defaultValue = "10") Long limit) {

		TagCollectionResource resource = new TagCollectionResource();

		resource.count = _tagRepository.count();

		// add HATEOAS links
		resource.add(linkTo(TagsController.class).withSelfRel());
		resource.add(linkTo(HomeController.class).withRel("up"));

		return ResponseEntity.ok().body(resource);
	}

}
