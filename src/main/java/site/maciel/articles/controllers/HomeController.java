package site.maciel.articles.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import site.maciel.articles.controllers.contract.resources.HomeResource;

@RestController
@RequestMapping(path = "/", produces = "application/hal+json")
public class HomeController {

	@GetMapping
	public ResponseEntity<HomeResource> Get() {
		HomeResource resource = new HomeResource();

		// add HATEOAS links
		resource.add(linkTo(HomeController.class).withSelfRel());
		resource.add(linkTo(ArticlesController.class).withRel("aa:articles"));
		resource.add(linkTo(TagsController.class).withRel("aa:tags"));

		return ResponseEntity.ok().body(resource);
	}

}
