# first stage: build
FROM maven:3.6.1-jdk-8 AS build

COPY pom.xml pom.xml
COPY src src
RUN mvn -Dmaven.test.skip=true package

# final stage: deploy and run
FROM openjdk:8-jdk-alpine

ARG ARTICLES_VERSION
ENV ARTICLES_VERSION 0.0.1-SNAPSHOT

ARG JAR_FILE
ENV JAR_FILE Articles-${ARTICLES_VERSION}.jar

LABEL org.label-schema.schema-version="1.0" \
    org.label-schema.name="Articles" \
    org.label-schema.version=${ARTICLES_VERSION} \
    org.label-schema.description="A small example of a Web API implemented in Spring Boot" \
    org.label-schema.url="https://gitlab.com/ruimaciel/articlesapi" \
    org.label-schema.vcs-url="git@gitlab.com:ruimaciel/articlesapi.git" \
    org.label-schema.vendor="Rui Maciel"

RUN addgroup -S articlesgroup && adduser -S articlesuser -G articlesgroup
USER articlesuser
WORKDIR /opt/articles

COPY --from=build target/${JAR_FILE} articles.jar

VOLUME /tmp
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/articles/articles.jar"]

EXPOSE 8080